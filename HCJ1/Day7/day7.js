function example3() {
    //"person1": "Arun",
    //    "emailAddress": "Arun@doxc",
    //    "code": "002",

    //    "person": "Saravana",
    //    "emailId": "Arun@doxc",
    //        "Accesscode": "002"

    obj = {};
    Object.defineProperty(obj, 'person1', { value: 'Arun' });
    Object.defineProperty(obj, 'emailAddress', { value: 'Arun@doxc' });
    Object.defineProperty(obj, 'code', { value: '002' });

    newObj = Object.create(obj);
    Object.defineProperty(newObj, 'person', { value: 'Saravana' });
    Object.defineProperty(newObj, 'emailId', { value: 'Saravana@doxc' });
    Object.defineProperty(newObj, 'Accesscode', { value: '0d02' });
    console.log(obj);
    console.log(newObj);

    //Logic 2
    var person = { name: "Arun", surname: "Kumar" }; //Prakash

    Object.defineProperty(person, 'fullName', {
        get: function () {
            console.log("from get");
            return this.name + " " + this.surname + " this is very bad or mad activities";
        }, //Readonly Property
        set: function (value) {           
            [this.name, this.surname] = value.split(" ");
            console.log("from set -", value);
        } //WriteOnly Property
    });
    //console.log(person.fullName); // -> "Arun Kumar"

    person.surname = "Prakash";
    //console.log(person.fullName); // -> "John Hill"

    person.fullName = "Arun Prakash"; //set
    console.log(person.name); // -> "Mary"
    console.log(person.surname);
    console.log(person.fullName); // get
}





//var userInfo = {
//    firstName: "",
//    lastName:"",
//    phoneNumber: "",
//    emailAddress:""
//};

//function objectOps() {
//    console.log('From object ops function');
//    userInfo.firstName = "Shanmugam";
//    userInfo.lastName = "R";
//    userInfo.emailAddress = "Shanmu21@hotmail.com";
//    userInfo.phoneNumber = "9987654369";
    
//    Object.assign(userInfo, { Dob: "30-02-2000",Address:"Chennai" });

//    usersArray = [];
//    usersArray.push(userInfo);


//    console.log(usersArray);
//}


//Object.assign(user, { lastName: "Doe", age: 39 });
//console.log(user);

//Example2

var employee = {
    employeeId : "",
    fName : "",
    emailId : ""
} // REFERENCE TYPE

var refId = 0; // Value type

function Example2() {
    employees = [];

    oldEmployee = Object.create(employee);
    

    oldEmployee.employeeId = "D001";
    oldEmployee.fName = "Arun";
    oldEmployee.emailId = "arun@doxc.in";
    refId = 10;

    employees.push(oldEmployee);
    employees.push(refId);

    newEmployee = Object.create(employee);

    newEmployee.employeeId = "D002";
    newEmployee.fName = "Saravanan";
    newEmployee.emailId = "Saravanan@doxc.in";
    refId = 2220;

    


    employees.push(newEmployee);
    employees.push(refId);

    let obj3 = { ...employees, userCode: "002" }
    console.log(obj3);
}
