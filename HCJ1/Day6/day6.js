function ColorLuminance(hex, lum) {
	console.log(hex, lum);
	// validate hex string
	hex = String(hex).replace(/[^0-9a-f]/gi, '');
	console.log(hex);
	if (hex.length < 6) {
		hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
	}
	lum = lum || 0;

	// convert to decimal and change luminosity
	var rgb = "#", c, i;
	for (i = 0; i < 3; i++) {
		c = parseInt(hex.substr(i * 2, 2), 16);
		c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
		rgb += ("00" + c).substr(c.length);
	}

	return rgb;
}

function changeColorDynamically() {	
	var x = Math.floor((Math.random() * 10) + 1);
	var y = Math.floor((Math.random() * 10) + 1);
	var z = Math.floor((Math.random() * 10) + 1);
	var ele = document.getElementById("dynamicColor");
	ele.style = "background:" + ColorLuminance(String(x) + String(y) + String(z) , 0);
}