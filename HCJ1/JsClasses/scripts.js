//Custom dateoperator
// used for return date as per the country standard
class customDateOperator
{
    assignedDate = "";

    constructor(date) {
        this.assignedDate= new Date(date);
    }
    //Indian Standard

    //USA

    //UK
}

function InvokeDateOperator() {
    var dataOperator = new customDateOperator(new Date(2020,4,20)); 
}

function CustomInvoke() {
    var mobile1 = new Mobile();
    var mobile2 = new Mobile();

    //first object
    mobile1.display_size = "6'";
    mobile1.name = "Nokia 8";
    mobile1.price = "12345";
    mobile1.manufacture = "Nokia";

    //second  object
    mobile2.display_size = "5'";
    mobile2.name = "Sumsung Glaxy 8";
    mobile2.price = "62345";
    mobile2.manufacture = "Sumsung";

    mobile1.checkMobiles(mobile2);

    mobile1.checkDisplay();
    mobile2.checkDisplay();

    console.log(mobile1);
    console.log(mobile2);
}