function changeValue()
{
    document.getElementById('greet').innerHTML = "This is client side values"
}

function loadNewImage()
{
    var img = document.getElementById('imgpic');
    img.src = "https://pluralsight2.imgix.net/paths/images/javascript-542e10ea6e.png";
    img.width = 125;
    img.height=150;
    console.log("img from the click url "+img.src);
}

function resetImage()
{
    var img = document.getElementById('imgpic');
    img.src = "https://code.visualstudio.com/assets/docs/languages/html/htmlintellisense.png";
    img.width = 225;
    img.height=450;
    console.log("img from the click url "+img.src);
}